var configDB = require('./configDB');
var database = require('./database');

var Friend = database.Friend;

module.exports = function () {
    if (configDB.seed) {
        Friend.bulkCreate([
            {name: "Peter Lim"  , email: "pete@gamil.com"  , contact: "123456789"}
            , {name: "Mary Sue" , email: "mary@yahoo.com"  , contact: "987654321"}
            , {name: "John Sim" , email: "john@hotmail.com", contact: "121245456"}
            , {name: "Ethan Tan", email: "ethan@sp.edu.sg" , contact: "998877665"}
        ]).then(function () {
            console.log("Done creating Friend records");
        });
    }
}
