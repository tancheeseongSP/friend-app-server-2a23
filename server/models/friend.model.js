var Sequelize = require('sequelize');

module.exports = function (database) {
    return database.define('friend', {
        fr_id : {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        name : {
            type: Sequelize.STRING(100),
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        },
        email : {
            type: Sequelize.STRING(100),
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        },
        contact : {
            type: Sequelize.STRING(20),
            primaryKey: false,
            autoIncrement: false,
            allowNull: false
        }
    }, {
        tableName: 'friends',
        timestamps: false
    });
}
