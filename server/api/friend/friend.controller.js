var Friend = require('../../database').Friend;

var friendList = [
    {  fr_id: 1, name: "Peter Lim", email: "pete@yahoo.com"  , contact: "123456"}
    , {fr_id: 2, name: "Mary Tan" , email: "mary@gmail.com"  , contact: "654321"}
    , {fr_id: 3, name: "John Wu"  , email: "john@hotmail.com", contact: "999888"}
    , {fr_id: 4, name: "John Wu2"  , email: "john2@hotmail.com", contact: "777888"}
];
var current_fr_id = 5;


exports.list = function (req, resp) {
    Friend.findAll(
        
    ).then(
        (allFriendRecords) => { 
            resp
                .status(200)
                .type('application/json')
                .json(allFriendRecords); 
        }
    ).catch(
        (error) => { 
            resp
                .status(500)
                .type('application/json')
                .json({error: true}) 
        }
    );
}

// exports.list = function (req, resp) {
//     resp.status(200);
//     resp.type("application/json");
//     resp.json(friendList);
// }

exports.add = function (req, resp) {
    if (!req.body.info) {
        console.log("No info Found!");
        resp.status(400).json({error: true});
    } else {
        var newFriendInfo = req.body.info;

        //console.log(newFriendInfo);
        delete newFriendInfo['fr_id'];
        //console.log(newFriendInfo);

        Friend.create(
            newFriendInfo
        ).then((newRecord) => {
            resp
                .status(200)
                .type('application/json')
                .json(newRecord);
        }).catch((error)=> {
            resp
                .status(500)
                .type('application/json')
                .json({error: true});
        });
    }
}

exports.remove = function (req, resp) {
    if (!req.params.fr_id) {
        resp.status(400).send("Bad Request");
    } else {
        var whereClause = {fr_id: parseInt(req.params.fr_id)};

        Friend.destroy(
            {where: whereClause}
        ).then((numOfRowDeleted) => {
            let status = (numOfRowDeleted == 1);
            resp
                .status(200)
                .type('application/json')
                .json({success: status});
        }).catch((error)=> {
            resp
                .status(500)
                .type('application/json')
                .json({error: true});
        });
    }
}
// For Update //////////////////////////////////////////////////
exports.update = function (req, resp) {
    if (!req.body.info) {
        resp.status(400).send("Bad Request");
    } else {
        var updateInfo = req.body.info;
        var whereClause = {fr_id: parseInt(req.params.fr_id)};

        Friend.update(
            updateInfo,
            {where: whereClause}
        ).then((result) => {
            let status = (result[0] == 1);

            resp
                .status(200)
                .type("application/json")
                .json({success: status});
        }).catch((error)=> {
            resp
                .status(500)
                .type('application/json')
                .json({error: true});
        });
    }
}
////////////////////////////////////////////////////////////////


