var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var cors = require('cors');

var friendCtrl = require('./api/friend/friend.controller');

var app = express()
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cors()); 

app.use(express.static(path.join(__dirname, "../public")));

app.get("/api/friends", friendCtrl.list); 
app.post("/api/friends", friendCtrl.add);
app.delete("/api/friend/:fr_id", friendCtrl.remove);

// For Update //////////////////////////////////////////////////
app.put   ("/api/friend/:fr_id", friendCtrl.update);
////////////////////////////////////////////////////////////////


app.use(function (req, resp) {
    resp.status(404);
    resp.send("Error File Not found");
});

app.listen('3000', function () {
    console.log("Server running at http://localhost:3000");
})